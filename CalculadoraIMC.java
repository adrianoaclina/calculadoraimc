import javax.swing.JOptionPane;

/**
 * Este código calcula o imc
 * 
 * @author Adriano Aclina
 *
 */
public class CalculadoraIMC {

	public static void main(String[] args) {

		// peso
		String peso = JOptionPane.showInputDialog(null, "Qual seu peso em KG: ", "Peso", JOptionPane.QUESTION_MESSAGE);

		// converte de String para double
		double pesoEmKg = Double.parseDouble(peso);

		// altura
		String altura = JOptionPane.showInputDialog(null, "Qual a sua altura: ", "Altura",
				JOptionPane.QUESTION_MESSAGE);
		double alturaEmMetros = Double.parseDouble(altura);

		// Calcula o imc
		double imc = pesoEmKg / Math.pow(alturaEmMetros, 2);

		// diagnóstico
		if (imc < 18.5) {
			// System.out.println("Abaixo do peso normal");
			JOptionPane.showMessageDialog(null, "IMC: " + imc + "\nAbaixo do peso normal", "Diagnóstico",
					JOptionPane.INFORMATION_MESSAGE);
		} else if (imc < 24.5) {
			// System.out.println("Peso normal");
			JOptionPane.showMessageDialog(null, "IMC: " + imc + "\nPeso normal", "Diagnóstico",
					JOptionPane.INFORMATION_MESSAGE);
		} else if (imc < 29.9) {
			// System.out.println("Excesso de peso");
			JOptionPane.showMessageDialog(null, "IMC: " + imc + "\nExcesso de peso", "Diagnóstico",
					JOptionPane.INFORMATION_MESSAGE);
		} else if (imc < 34.9) {
			// System.out.println("Obesidade classe 1");
			JOptionPane.showMessageDialog(null, "IMC: " + imc + "\nObesidade classe 1", "Diagnóstico",
					JOptionPane.INFORMATION_MESSAGE);
		} else if (imc < 39.9) {
			// System.out.println("Obesidade classe 2");
			JOptionPane.showMessageDialog(null, "IMC: " + imc + "\nObesidade classe 2", "Diagnóstico",
					JOptionPane.INFORMATION_MESSAGE);
		} else {
			// System.out.println("Obesidade classe 3");
			JOptionPane.showMessageDialog(null, "IMC: " + imc + "\nObesidade classe 3", "Diagnóstico",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}
}
